package com.example.romanko.currency.views

import android.os.Handler
import android.os.Looper
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.romanko.currency.R
import com.example.romanko.currency.data.Currency
import kotlinx.android.synthetic.main.cell_currency.view.*

class CurrenciesAdapter(val currencyClick: (Currency) -> Unit) : RecyclerView.Adapter<CurrenciesAdapter.ViewHolder>() {

    var currencies = ArrayList<Currency>()
    var base = Currency("fake")

    companion object {
        val ANIMATION_DURATION = 300L //based on RecyclerView.ItemAnimator src
    }

    val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            //empty
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            //empty
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (!s.isNullOrEmpty()) {
                try {
                    base.rate = s.toString().toFloat()
                } catch (e: NumberFormatException) {
                    base.rate = 0f
                }
            } else {
                base.rate = 0f
            }
            if (s.isNullOrEmpty() || (s.toString().get(0) == '0' && s.toString().length > 1)) {
                notifyDataSetChanged()
            } else {
                notifyItemRangeChanged(1, currencies.size - 1)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.cell_currency, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData()
    }

    override fun getItemCount(): Int = currencies.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindData() {
            val currency = currencies[adapterPosition]
            val newRate = if (adapterPosition == 0) base.rate else currency.rate * base.rate
            itemView.etInput.removeTextChangedListener(textWatcher)

            itemView.tvTitle.text = currency.name
            itemView.tvDescription.text = currency.displayName
            itemView.etInput.setText(itemView.context.getString(R.string.currency_format, newRate))
            itemView.image.setImageURI("asset:///${currency.name.substring(0, 2).toLowerCase()}.png")

            if (adapterPosition == 0) {
                itemView.etInput.addTextChangedListener(textWatcher)
            }

            itemView.etInput.setOnFocusChangeListener { _, hasFocus ->
                if (adapterPosition == 0) return@setOnFocusChangeListener
                if (hasFocus) {
                    currency.rate = itemView.etInput.text.toString().toFloat()
                    currencyClick(currency)
                }
            }

            itemView.setOnClickListener {
                if (adapterPosition == 0) return@setOnClickListener
                itemView.etInput.requestFocus()
            }
        }
    }

    fun updateCurrencies(newCurrencies: ArrayList<Currency>, base: Currency) {
        for (currency in newCurrencies) {
            val index = currencies.indexOf(currency)
            if (index >= 0) {
                currencies[index].rate = currency.rate
            } else {
                currencies.add(currency)
            }
        }
        if (base == this.base) {
            notifyItemRangeChanged(1, currencies.size - 1)
            return
        }
        this.base = base
        val baseIndex = currencies.indexOf(base)
        if (baseIndex >= 0) {
            notifyItemMoved(currencies.indexOf(base), 0)
            currencies.removeAt(baseIndex)
            currencies.add(0, base)
            //Let's be honest. It's a hack. However, in terms of time shortage it works ok
            Handler(Looper.getMainLooper()).postDelayed({ notifyDataSetChanged() }, ANIMATION_DURATION)
        } else {
            currencies.add(0, base)
            notifyDataSetChanged()
        }
    }
}