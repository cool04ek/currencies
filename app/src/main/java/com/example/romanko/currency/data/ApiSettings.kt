package com.example.romanko.currency.data


object ApiSettings {
    const val SCHEME = "https://"

    const val HOSTNAME = "revolut.duckdns.org/"

    const val SERVER = SCHEME + HOSTNAME

    const val CURRENCIES = "latest"
}