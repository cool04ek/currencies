package com.example.romanko.currency.views

import android.content.Context
import com.example.romanko.currency.data.Currency


interface CurrenciesView {
    fun showCurrencies(currencies: ArrayList<Currency>, base: Currency)
    fun showError(error: String)
    fun context(): Context
}