package com.example.romanko.currency.data

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.net.URLDecoder


class CurrencyResponseDeserializer : JsonDeserializer<CurrenciesResponse> {

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): CurrenciesResponse {
        val base = json.asJsonObject.get("base").asString
        val ratesArray = json.asJsonObject.getAsJsonObject("rates")
        val rates = ArrayList<Currency>()
        ratesArray.entrySet().mapTo(rates) { Currency(it.key, it.value.asFloat) }

        return CurrenciesResponse(Currency(base), rates)
    }
}