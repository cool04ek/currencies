package com.example.romanko.currency.data

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface CurrencyService {
    @GET(ApiSettings.CURRENCIES)
    fun getCurrencies(@Query("base") base: String): Single<CurrenciesResponse>
}