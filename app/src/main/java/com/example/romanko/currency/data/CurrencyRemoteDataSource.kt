package com.example.romanko.currency.data

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class CurrencyRemoteDataSource {
    lateinit var currencyService: CurrencyService
    private lateinit var retrofit: Retrofit

    fun init() {
        initRetrofit()
        initServices()
    }

    private fun initServices() {
        currencyService = retrofit.create<CurrencyService>(CurrencyService::class.java)
    }

    private fun initRetrofit() {
        val client = OkHttpClient.Builder().addInterceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                    .method(original.method(), original.body())
                    .build()
            chain.proceed(request)
        }.build()

        retrofit = Retrofit.Builder()
                .baseUrl(ApiSettings.SERVER)
                .addConverterFactory(createGsonConverter())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
    }

    private fun createGsonConverter(): GsonConverterFactory {
        val builder = GsonBuilder()
        builder.serializeNulls()
        builder.registerTypeAdapter(Class.forName("com.example.romanko.currency.data.CurrenciesResponse"), CurrencyResponseDeserializer())
        return GsonConverterFactory.create(builder.create())
    }
}