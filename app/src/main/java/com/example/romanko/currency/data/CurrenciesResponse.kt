package com.example.romanko.currency.data


data class CurrenciesResponse(var base: Currency, var rates: ArrayList<Currency>)