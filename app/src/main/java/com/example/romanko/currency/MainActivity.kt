package com.example.romanko.currency

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.example.romanko.currency.data.Currency
import com.example.romanko.currency.presenter.CurrencyPresenter
import com.example.romanko.currency.views.CurrenciesAdapter
import com.example.romanko.currency.views.CurrenciesView
import com.facebook.drawee.backends.pipeline.Fresco
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), CurrenciesView {

    lateinit var presenter: CurrencyPresenter
    val adapter = CurrenciesAdapter({ currency -> currencyClick(currency) })
    lateinit var baseCurrency: Currency

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun showCurrencies(currencies: ArrayList<Currency>, base: Currency) {
        base.rate = baseCurrency.rate
        adapter.updateCurrencies(currencies, base)
        if (!presenter.timerRuns) {
            rvCurrencies.scrollToPosition(0)
        }

        presenter.startTimer(base)
    }

    override fun context() = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fresco.initialize(this);
        setContentView(R.layout.activity_main)

        rvCurrencies.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvCurrencies.adapter = adapter
        baseCurrency = Currency()
    }

    override fun onStart() {
        super.onStart()
        presenter = CurrencyPresenter()
        presenter.attachView(this)
        presenter.getCurrencies(baseCurrency.name)
    }

    override fun onStop() {
        super.onStop()
        presenter.stopTimer()
        presenter.detachView()
    }

    private fun currencyClick(currency: Currency) {
        presenter.stopTimer()
        baseCurrency = currency
        presenter.getCurrencies(currency.name)
    }
}
