package com.example.romanko.currency.data


data class Currency(var name: String = "EUR", var rate: Float = 1f, var displayName: String = "") {
    override fun equals(other: Any?): Boolean {
        if (other is Currency) {
            return name == other.name
        } else return false
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}