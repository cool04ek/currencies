package com.example.romanko.currency.presenter

import android.util.Log
import com.example.romanko.currency.MainActivity
import com.example.romanko.currency.data.CurrenciesResponse
import com.example.romanko.currency.data.Currency
import com.example.romanko.currency.data.CurrencyRemoteDataSource
import com.example.romanko.currency.views.CurrenciesView
import io.reactivex.SingleObserver
import io.reactivex.SingleSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*


class CurrencyPresenter {
    companion object {
        val TIMER_RATE = 1000L
    }

    private var view: CurrenciesView? = null
    private val disposables = CompositeDisposable()

    private lateinit var timer: Timer
    private lateinit var timerTask: TimerTask
    var timerRuns = false

    private val currencyRemoteDataSource: CurrencyRemoteDataSource = CurrencyRemoteDataSource().apply {
        init()
    }

    fun attachView(view: CurrenciesView) {
        this.view = view
    }

    private fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    fun detachView() {
        disposables.dispose()
        view = null
    }

    fun getCurrencies(base: String) {
        addDisposable(currencyRemoteDataSource.currencyService.getCurrencies(base)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map<CurrenciesResponse> { t: CurrenciesResponse ->
                    for (currency in t.rates) {
                        currency.displayName = java.util.Currency.getInstance(currency.name).displayName
                    }
                    t.base.displayName = java.util.Currency.getInstance(t.base.name).displayName
                    CurrenciesResponse(t.base, t.rates)
                }
                .subscribe({ result ->
                    view?.showCurrencies(result.rates, result.base)
                }, { error ->
                    error.printStackTrace()
                    view?.showError(error.localizedMessage)
                }))
    }

    fun startTimer(base: Currency) {
        if (timerRuns) return
        timer = Timer()
        timerTask = object : TimerTask() {
            override fun run() {
                getCurrencies(base.name)
            }
        }
        timer.scheduleAtFixedRate(timerTask, TIMER_RATE, TIMER_RATE)
        timerRuns = true
    }

    fun stopTimer() {
        timer.cancel()
        timerRuns = false
    }
}